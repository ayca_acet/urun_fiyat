
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from sklearn.preprocessing import PolynomialFeatures



veri = pd.read_csv("EURO.csv")


x = veri["GÜN"]
y = veri["FİYAT"]
Sonuc=0 


x = x.values.reshape(252,1)   # pandas kullanımında veri alırken sıkıntı oluşturmamak için 
y= y.values.reshape(252,1)

plt.scatter(x,y)       # noktalı grafik şekil 
plt.show() 

#Linear Reg. modeli

tahmin = LinearRegression()     
tahmin.fit(x,y)    # elimdeki verileri x ve y düzleme entegre ediyorum
tahmin.predict(x)  # (x=gün)  Günlere göre fiyat tahmini (x eksenine göre y ekseni tahmini)


plt.plot(x,tahmin.predict(x),c="black")  # linear modeline göre tahmini

#Polinom Reg. için model 

tahminpolinom8 = PolynomialFeatures(degree=3) 
Xyeni =tahminpolinom8.fit_transform(x)  # Günlere göre fiyat tahmini tahmin  

polinomModel = LinearRegression()
polinomModel.fit(Xyeni,y)
polinomModel.predict(Xyeni) # Xyeni göre tahmin     

plt.plot(x,polinomModel.predict(Xyeni)) #polinom modeline göre tahmini
plt.show()





# modellerin hata tespiti için test

hataLineer = 0
hataPolinom = 0

# linear reg. için hata
for i in range(len(y)):
    hataLineer = hataLineer + (float(y[i])-float(tahmin.predict(x)[i]))**2
    # gerçek değerim y[i], tahmini değer x[i]

 # polinom reg. için hata 
for i in range(len(Xyeni)):
    hataPolinom = hataPolinom + (float(y[i])-float(polinomModel.predict(Xyeni)[i]))**2
    print(hataPolinom)
    # gerçek değerim y[i] , tahmini değer Xyeni[i] ve 






for a in range(252):  # 252 gün için tek tek tahmin dönen for döngüsü
   sonuc =(float(y[a])-float(polinomModel.predict(Xyeni)[a]))  # tahmini sonucum
print(sonuc)
sonuc=0


   